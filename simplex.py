from __future__ import division
from numpy import *
import time

class Table:

    def __init__(self, obj):
        self.obj = [1] + obj
        self.rows = []
        self.cons = []
#		iterat=0
 
    def add_constraint(self, expression, value):
        self.rows.append([0] + expression)
        self.cons.append(value)
 
    def _pivot_column(self):
        low = 0
        idx = 0
        for i in range(1, len(self.obj)-1):
            if self.obj[i] < low:
                low = self.obj[i]
                idx = i
        if idx == 0: return -1
        return idx
 
    def _pivot_row(self, col):
        rhs = [self.rows[i][-1] for i in range(len(self.rows))]
        lhs = [self.rows[i][col] for i in range(len(self.rows))]
        ratio = []
        for i in range(len(rhs)):
            if lhs[i] == 0:
                ratio.append(99999999 * abs(max(rhs)))
                continue
            ratio.append(rhs[i]/lhs[i])
        return argmin(ratio)
 
    def display(self):
        print '\n', matrix([self.obj] + self.rows)
 
    def _pivot(self, row, col):
        e = self.rows[row][col]
        self.rows[row] /= e
        for r in range(len(self.rows)):
            if r == row: continue
            self.rows[r] = self.rows[r] - self.rows[r][col]*self.rows[row]
        self.obj = self.obj - self.obj[col]*self.rows[row]
 
    def _check(self):
        if min(self.obj[1:-1]) >= 0: return 1
        return 0

    def iter(self,iteration):
        self.iteration=0
        self.iteration+=1
        return self.iteration

    def solve(self):
 
        # build full table
        for i in range(len(self.rows)):
            self.obj += [0]
            ident = [0 for r in range(len(self.rows))]
            ident[i] = 1
            self.rows[i] += ident + [self.cons[i]]
            self.rows[i] = array(self.rows[i], dtype=float)
        self.obj = array(self.obj + [0], dtype=float)
 
        # solve
        self.display()
        iteration=1
        while not self._check():
            c = self._pivot_column()
            r = self._pivot_row(c)
            self._pivot(r,c)
            print '\npivot column: %s\npivot row: %s'%(c+1,r+2)
            #print ("iteration number: ", iter(self.iteration))
            print ("Iteration number: %i"%iteration)
            iteration+=1
            if iteration>1000:
                print ("\n------------------------------")
                print ("Problem is unsolved.")
                print ("------------------------------\n")
                return -1
            self.display()


if __name__ == '__main__':
 
    """
    max z = 2x + 3y + 2z
    st
    2x + y + z <= 4
    x + 2y + z <= 7
    z          <= 5
    x,y,z >= 0
    """
    # ONE
    # t = Table([-2,-3,-2])
    # t.add_constraint([2, 1, 1], 4)
    # t.add_constraint([1, 2, 1], 7)
    # t.add_constraint([0, 0, 1], 5)
#
    # TWO
    # t = Table([-15,-10,])
    # t.add_constraint([1, 2], 2)
    # t.add_constraint([0, 1], 3)
    # t.add_constraint([1, 1], 4)

    #THREE
    # t = Table([-5,-25,])
    # t.add_constraint([2, 1], 12)
    # t.add_constraint([2, 2], 20)

    #FOUR
    t = Table([-3,-5,])
    t.add_constraint([3, 1], 30)
    t.add_constraint([1, 2], 20)
    t.add_constraint([1, 1], 13)

    # FIVE
    # t = Table([-1000,-3000,])
    # t.add_constraint([3, 1], 33000)
    # t.add_constraint([1, 1], 13000)
    # t.add_constraint([5, 8], 80000)
    # t.add_constraint([0, 2], 7000)

     # SIX

    #filenames = [ 'dane_%d.txt'%i for i in range(1,10)]
    filenames = ['szympleks.txt']

    for filename in filenames:
        table=[]
        constraints=[]
        f = open(filename)
        table = map(int,f.readline().split(','))
        constraints = [[int(x) for x in line.split(',')] for line in f]
        f.close()

        t = Table(table)
        for row in constraints:
            t.add_constraint(row[:-1], row[-1])

        start=time.clock()
        t.solve()
        stop=(time.clock()-start) *1000
        print '-------------------------------------------'
        print 'czas wykonywania kodu: %f ms'%stop
